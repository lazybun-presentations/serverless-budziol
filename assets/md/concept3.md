### Problems

* Stop API Gateway from killing budziol lambda <!-- .element: class="fragment" -->
* Deliver image link to frontend when it's done <!-- .element: class="fragment" -->

Note:
I've had to avoid API Gateway timeout. To do that I've decided to change the synchronous request to fire and forget request. But now, I've had to get the image to frontend somehow...

---

<img src="assets/image/diag31.png" style="width: 100%; background-color: white"></div>

Note:
I though about using SQS to achieve that, but as it turned out it's impossible to use sqs in frontend javascript so I dropped the idea. Now, keep in mind that this project was supposed to be...

---

<img src="assets/image/in-and-out.gif" style="width: 50%"></div>

Note:
20 minute adventure and I've been at it for several days coding in spare time. So instead of looking for a solution in AWS, I've turned to

---

<img src="assets/image/pusher.png" style="width: 50%"></div>

Note:
Pusher. Pusher is a simple websocket server as a service. It has a neat free plan which I decided to use. And, finally, I've arrived to my final solution

---

<img src="assets/image/diag32.png" style="width: 100%; background-color: white"></div>

Note:
A little bit of explanation. Frontend generated a session id (UUID), connected to websocket room with this id and passed this id along with image to lambda. Then processing lambda sent messages via websocket on given session id, like processing started, processing failed, processing completed. I think this solution worked great and was realitively secure as well! In addition to that, thanks to making budziol asynchronous, now several images could be processed in parallel. Finaly I've added some polish, quality of life improvements and decided that it's time finish and deploy the project.

## Some stats

<img src="assets/image/stats1.png" style="width: 70%"></div>

---

## Final thoughts

Note:
If I learned anything during the aforementioned JDD17 talk, it was that creating serverless applications often leads to overly complex application architecture. The Budziol project is a perfect example of that. If I used a VPS or something similar, I could have stopped at the first concept and call it a day.
However, during Budziol development I came to a realization that complexity introduced by AWS was not necessarily a bad thing. If I stuck to the first concept, I would have an application capable of resizing a single image at a time, with the image returned as a binary response, instead of a link you could easily share with your friends.
Thanks to the imitations AWS set on me, I had to come up with more complex, but objectively better architecture for a simple application, which enhanced it in ways I didn't think about when starting to work on Budziol. 
So, in the end, would I use Lambda at work? Sure. If we have a good use case for it, it can be a fantastic tool. I suggest you give it a try if you can come up with a worthwhile idea to use it with.

---

### Thank you!

@fa[twitter gp-contact]() @leniwabula

@fa[github gp-contact]() lazybun

@fa[gitlab gp-contact]() lazybun

@fa[envelope gp-contact]() krzysztof@piszkod.pl

Note:
Pytania:
Jak zablokować aws po przekroczeniu darmowego limitu?

<img src="assets/image/test.gif" style="width: 50%"></div>

Note:
I sent the application link to several friends and waited for feedback. Aside from all the laughs we had, I recognized two issues:

---

### Feedback

* Images in lossy formats took extremely long to process <!-- .element: class="fragment" -->
* Images took too long to process and the processing time wasn't clearly communicated <!-- .element: class="fragment" -->

Note:
Complaints regarding image processing times were something I expected, but what surprised me was that lossy images took longer to process. It must be because Caire takes extra steps for compressed images, but I couldn't find any specific piece of Caire code that stood out to me.
I resolved both of these issues by adding the speedup option which, if active, resized the image and converted it to loseless PNG format.

<img src="assets/image/diag2.png" style="width: 100%; background-color: white"></div>

Note:
To fix a binary response issue I've decided to just return a link to the image, which lambda would store to S3 after processing. It actually improved the application - now you could send image link to someone! And of course, since I wanted to try AWS services, using S3 is a nice bonus as well.
Mention expiry policy

---

<img src="assets/image/gotta-code-fast.jpg" style="width: 70%"></div>

Note:
And so I got to coding again. And it looked like everything worked! My test image got rescaled and returned to frontend. I got hyped and asked my brother for a picture. It was a MUCH bigger picture than I've used. And of course, I've run into even more issues!

---

### More issues

* AWS Lambda doesn't destroy lambda instance every time <!-- .element: class="fragment" -->
* AWS Lambda times out after 5 minutes <!-- .element: class="fragment" -->
* API Gateway times out after 30 seconds <!-- .element: class="fragment" -->

Note:
I had an issue where lambda would return image I previously sent do it.
As it turnes out, AWS doesn't create new lambda every execution. If there are several requests close to each other, they will actually execute on the same lambda instance persisting files between runs! This one took me a while to debug. But more importantly...

This limitation is well known - after 1st concept's failure a read some documentation (wow!) and was aware of it. I really couldn't do anything about, so I've just added an information to frontend about this limitation. There was another, bigger issue though.

Now this, I didn't know about. It turns out that after 30 seconds API gateway will timeout and kill the lambda it was executing as a result.  Caire requires some time to process images even on my beast of a workstation, so 30 seconds simply wouldn't fly. AND my concept was synchronous, as I had to show image on frontend somehow. It was clear that I needed to make it asynchronous.

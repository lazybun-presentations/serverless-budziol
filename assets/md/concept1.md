### Assumptions

* Use AWS <!-- .element: class="fragment" -->
* Everything should be free <!-- .element: class="fragment" -->

Note:
I've set out with two concepts in mind:
Now please note, that it is a side project. And when I do side projects I like to just get to it, without much planning - I have enough of that at work. Hence my first concept was pretty basic

---

<img src="assets/image/diag1.png" style="width: 100%; background-color: white"></div>

Note:
So: Frontend receives an image from the user which is sent to Budziol Lambda via API Gateway, and, as a result, transformed image is returned. Looks simple enough, right? …Right?

---

## [python-λ](https://github.com/nficano/python-lambda)

Note:
I decided to code it in Python for no reason in particular. With helper toolbox python lambda, I've quickly coded the first concept. 
Remember when I told you that I just wanted to get to coding? Well..

---

<img src="assets/image/hindsight.jpg" style="width: 50%"></div>

Note:
In hindsight maybe I should have planned a little bit more and read some docs, as first issues with my solution arrived pretty quickly

---

### First issues

* AWS Lambda requires the user to put all executable files into /tmp folder <!-- .element: class="fragment" -->
* Python lambda runtime does not allow for binary responses (at time of coding) <!-- .element: class="fragment" -->

Note:
I ran into an issue where I couldn't run the Caire executable from the default Lambda directory. Lambda requires you to put files into `/tmp` folder, so I moved Caire and the problem's been solved. In hindsight, I should have written the main lambda in Go and used Caire as a library instead of using executable file -  a classic example of RTFM.
At the beginning, I wanted to simply return images as binary responses but it quickly became apparent that wouldn't fly. To fix this issue, I decided to modify Budziol's architecture